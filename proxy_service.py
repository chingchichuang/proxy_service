from flask import Flask
import os

app = Flask(__name__)
MODEL_HOST = os.environ.get("MODEL_HOST")


@app.route("/")
def hello_world():
    return f"Hello world! MODEL_HOST is located at {MODEL_HOST}"


if __name__ == '__main__':
    app.run("0.0.0.0", 8080, use_reloader=False)